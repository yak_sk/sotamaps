This is a meta project demonstrating the functionality of Flask
to work as middleware for several applications.

In this case we are using the DispatcherMiddleware which allows
us to serve 2 different applications in two different /urls.
